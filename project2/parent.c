# include <stdio.h> 
# include <stdlib.h> 
# include <sys/types.h> 
# include <unistd.h> 
# include <ctype.h>

int main(int argc, char *argv[]){ 
        int  a,b, sleeptime;
        int i;
        int actualTime;
        pid_t childpid;
	char*end;	
        if (argc !=4) { 
                printf("\nThe program needs 3 arguments\n"); 
                exit(1);} 
        
        sleeptime = atoi(argv[1]);
	a = strtol(argv[2],&end,10);
	if(end==argv[2]){
		printf("\nNot a correct input for the first number\n");
		exit(0);
	}
        b = atoi(argv[3]);
        // Checks sleeptime input
        if(sleeptime <=0 || sleeptime > 50){
                printf("\nNot a correct input for the time\n");
                exit(0);
        }
//	if( isalpha(a) ){
//		printf("\nNot a correct input for the first number\n");
//		exit(0);	
//	}
	
	if(b==0){
		printf("\nNot a correct input for the second number\n");
		exit(0);
        }
        //checks second number input
        //prints the parent information
        printf("\nI am parent process %ld, max sleep time is %d, two numbers are %d and %d\n",
                (long)getpid(),sleeptime, a,b);

        srand(time(NULL));
        actualTime = rand() % sleeptime + 1;
	sleep(actualTime);
                
        for (i =0; i<4; i++){
                //array of 5 characters
                char number[5];
                //makes ints into chars 
                sprintf(number,"%d",i);
                //child forks as it goes into the switch statement
                switch(childpid=fork()){
                        //only goes here if the fork was successful and it is the child process
                        case 0:
        			sleep(actualTime);
                                //execs the child process to run the child program 
                                execlp("./child","child",argv[1],argv[2],argv[3],(char*)number,(char*)NULL);
                                perror("Exec fail"); return(1);
                        //this is an unsuccessful fork       
                        case -1:perror ("\nFork failure\n");
                                return(2);
                        //goes to default when dealing with the parent
                        default:printf("\nforked child %ld\n",(long)childpid);
                         //       sleep(actualTime);
                                continue;}
		sleep(actualTime);
        }
}

