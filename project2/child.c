# include <stdio.h> 
# include <stdlib.h> 
# include <sys/types.h> 
# include <unistd.h> 

int main(int argc, char *argv[]){ 
        int sleeptime,a,b,i;
        int result, remainder;
        int actualTime; 
        //gets the inputs from the parent and stores them into local variables
        sleeptime = atoi(argv[1]);
        a = atoi(argv[2]); 
        b = atoi(argv[3]);
        i = atoi(argv[4]);
        
        srand(time(NULL));
        actualTime = rand() % sleeptime + 1;
        
        sleep(actualTime);
        // the switch case is chosen depending on what it was in the for loop
        switch(i){
                case 0: 
                //addition
			result = a+b;
                        printf("\nI am child number %d, with PID %ld, the sum is %d \n", i,(long) getpid(), result);
                        break;
                case 1: 
                //subtraction
			result = a-b;
                        printf("\nI am child number %d, with PID %ld, the different is %d \n", i,(long) getpid(), result);
                        break;
                case 2: 
                  //multiplication
			result = a*b;
                        printf("\nI am child number %d, with PID %ld, the product is %d \n", i,(long) getpid(), result);
                        break;
                case 3: 
                //division
			result = a/b;
                        remainder = a%b;
                        printf("\nI am child number %d, with PID %ld, the quotient is %d with the remainder is %d \n",
                                         i,(long) getpid(), result, remainder);
                        break;
                default: perror("error");exit(1);}
}
