# include <stdio.h> 
# include <sys/types.h> 
# include <sys/ipc.h> 
# include <sys/sem.h> 
# include <unistd.h> 
# include <stdlib.h>
# include <time.h> 
#include <ctype.h>

union semun { 
	int val;
	struct semid_ds *buf;
	ushort *array; 
};
 
int main(int argc, char *argv[]) 
{
//initiate time 
	time_t mytime;
	char rem,tmp2,tmp3;
	int NS,k,j,tmp;
	int sem_id, sem_value, i;
	key_t ipc_key;
	struct semid_ds sem_buf;
//take off static infront of ushort because the array is dynamic now 	 
	ushort *sem_array;
	union semun arg; 
	ipc_key = ftok(".", 'S');
	if(argc < 3)
	{
		printf("Not enough arguments.\n");
		exit(0);
	}
//pointer to the letter 
	rem = *argv[1];
	if (rem != 'r')
	{
		if(rem != 'n')
		{
			printf("Wrong letter input\n");
			exit(0);
		}
	}
//number of semaphores being created
	tmp3 = *argv[2];
	if (isalpha(tmp3))
	{
		printf("NS is a letter value\n");
		exit(0);
	}
	NS= atoi(argv[2]);
	mytime = time(NULL);
//allocates memory for the array
//sem_array is a ushort pointer type int
	sem_array = (ushort*)malloc(NS*sizeof(int));
//makes sure there is enough arguments
//also checks num arguments match the num arguments specified
	if(argc != (NS+3))
	{
		printf("\nArguments not matched.Number of semaphores is %d\n",NS);
	//	if((argc-3) != NS)
	//	{
	//		printf(" Number of semaphores is %d\n",NS);
	//	}
		exit(0);
	}
//makes sure NS is positive
	if(NS < 0)
	{
		printf("\nIncorrect value for NS\n");
		exit(0);
	}
//array that takes in the arguments for the semaphores
	for (k=0; k<NS ; k++)
	{
		tmp2 = *argv[k+3];
		if ( isalpha(tmp2))
		{
			printf("Value is a letter \n");
			exit(0);
		}
		tmp = atoi(argv[k+3]);
		if (tmp <0 )
		{
			printf("Value is less than zero\n");
			exit(0);	
		}
		sem_array[k]= tmp;
		//sem_array[k] = atoi(argv[k+3]);
	} 	 


 /* Create semaphore */ 
//IPC_CREAT and IPC_EXCL when combined check for any previous semaphores
	if ((sem_id = semget(ipc_key, NS, IPC_CREAT | IPC_EXCL |0666)) == -1) 
	{
		perror ("semget: "); 
		exit(1); 
	} 
	printf ("Semaphore identifier %d\n", sem_id); 
 /* Set arg (the union) to the address of the storage location for */ 
 /* returned semid_ds value */ 
 
	arg.buf = &sem_buf; 
	if (semctl(sem_id, 0, IPC_STAT, arg) == -1) 
	{ 
		perror ("semctl: IPC_STAT"); 
		exit(2); 
	} 
//	printf ("Create %d\n", ctime(&sem_buf.sem_ctime)); 
//prints out time semaphore was created
	printf ("Create %s\n", ctime(&mytime));
 /* Set arg (the union) to the address of the initializing vector */ 
	arg.array = sem_array; 
 
	if (semctl(sem_id, 0, SETALL, arg) == -1) 
	{ 
		perror("semctl: SETALL"); 
		exit(3); 
	} 
	for (i=0; i<NS; ++i) 
	{ 
		if ((sem_value = semctl(sem_id, i, GETVAL, 0)) == -1) 
		{ 
			perror("semctl : GETVAL"); 
			exit(4); 
		} 
		printf ("Semaphore %d has value of %d\n",i, sem_value); 
	} 
 /* remove semaphore */
//if the letter input is r then it will remove the semaphore
	if(rem == 'r')
	{
		if (semctl(sem_id, 0, IPC_RMID, 0) == -1) 
		{ 
			perror ("semctl: IPC_RMID"); 
			exit(5); 
		}
	}	 
} 
