# include <stdio.h> 
# include <sys/types.h> 
# include <sys/ipc.h> 
# include <sys/sem.h> 
# include <unistd.h> 
# include <stdlib.h>
# include <time.h> 
# include <ctype.h>
# include <errno.h>
void set_sembuf_struct(struct sembuf*s,int num, int op,int flg);
int main(int argc, char *argv[])
{
	int semid,semop_ret,N,k,i ;
	char opt;
	struct sembuf semwait[1];
	struct sembuf semsignal[1];
	pid_t childpid;

	if (argc != 4)
	{
		printf("\nIncorrect number of arguments\n");
		exit(0);
	}
	N = atoi(argv[1]);
	if(N<0)
	{
		printf("\nInvalid input\n");
		exit(0);
	}
	opt = *argv[2];
	if( opt != 'n')
	{
		if(opt != 's')
		{
			printf("\nwrong letter input\n");
			exit(0);
		}
	}
	k = atoi(argv[3]);
	if(k<0)
	{
		printf("\nInvalid input\n");
		exit(0);
	}
	set_sembuf_struct(semwait,0,-1,0);
	set_sembuf_struct(semsignal,0,1,0);

	
	if (semop(semid, semsignal, 1) == -1) 
	{
		printf ("%ld: semaphore increment failed - %d\n", (long)getpid(), strerror(errno));
		if (semctl(semid, 0, IPC_RMID) == -1)
			printf ("%ld: could not delete semaphore - %d\n", (long)getpid(), strerror(errno));
	exit(1);
	}
	while (( (semop_ret = semop(semid, semwait, 1) ) == -1) && (errno ==EINTR));
	if (semop_ret == -1)
		printf ("%ld: semaphore decrement failed - %d\n", (long)getpid(), strerror(errno));
	else
	{
		printf("\nIt works\n");
		for(i=0;i<N;i++)
		{
			
		}
	}
	while (((semop_ret = semop(semid, semsignal, 1)) == -1) && (errno == EINTR));
	if (semop_ret == -1)
			printf ("%ld: semaphore increment failed - %d\n", (long)getpid(), strerror(errno));
	

}
void set_sembuf_struct(struct sembuf*s,int num, int op, int flg)
{
	s->sem_num = (short)num;
	s->sem_op = op;
	s->sem_flg = flg;
	return;

};



