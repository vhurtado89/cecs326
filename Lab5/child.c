#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

main(int argc,char * argv[])
{
	pid_t pid;
	int ret_value,time,in;
	pid = getpid();
	ret_value=(int)(pid%256);
//	in=atoi(argv[2]);
	time=atoi(argv[3]);
	srand((unsigned)pid);
	//makes the child sleep for the specified time 
	sleep(rand()%time);
	if(atoi(*(argv+1))%2){
		printf("Child %d is terminating with signal 009\n",pid);
		kill(pid,9);
	}else{
		printf("Child %d is terminating with exit(%04X)\n",pid,ret_value);
		exit(ret_value);
	}
}
