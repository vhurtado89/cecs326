#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

main(int argc,char* argv[])
{
	pid_t pid,w;
	int k,status,in,time;
	char * end;
	char * end2;
	char value[20];
	//checks for the correct number of arguments
	if(argc != 3){
		printf("\nnot enough arguments\n");
		exit(1);
	}
	//first input argument
	in=strtol(argv[1],&end,10);
	if(in >= 20 || in <=0 ||end == argv[1])
	{
		printf("\nCan't produce that much children\n");
		exit(0);
	}
	//second input argument
	time=strtol(argv[2],&end2,10);
	if(time >=50||time<=0|| end2 == argv[2])
	{
		printf("\nTIME input doesn't make sense\n");
		exit(0);
	}
	//forks number of children specified in the argument 
	for(k=0;k<in;++k){
		//if it the child it goes through this if
		if((pid=fork())==0){
			//converts integer to char
			sprintf(value,"%d",k);
			//makes new process 
			execl("child","child",value,argv[1],argv[2],(char *)0);
		}
		//if it is the parent running this command it will run this code
		else printf("Forked child %d \n",pid);
	}
	//wait for the childrean
	while((w=wait(&status)) && w!=-1){
		if(w!=-1) printf("Wait on PID: %d returns status of: %04X\n",w,status);
	}
	//exits the program as the parent 
	exit(0);
}
