/************************************************************************************/
/*  PROGRAM: project 1                                                                 */
/*  DESCRIPTION:                         */
/************************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>

int main(int argc, char *argv[])
{
	int i,n;
	//These are our children
	pid_t childone,childtwo;
	//checking for the second argunment input
        if(argc!=2){ 
  		printf("\nUsage: %s process \n", argv[0]);
		exit(1);
	}
	n = atoi(argv[1]);
	// making sure that the input is more than 0 and less than 5
	if(n<=0 || n>=5){
		printf("\nWrong entry\n");
		exit(1);
	}	
	childone=0;
	childtwo=0;
	printf("\nLevel  Process ID   Parent ID   Child1 ID  Child2 ID\n");
	for(i=0;i<n;i++){
		if(childone=fork()){//first child 
			if (childtwo=fork()){//second child 
			//makes the parent sleep so that the children can fork
			//once the children fork the parent can go to sleep
			sleep(i);
			printf(" \n %d     %6ld     %6ld      %6ld       %6ld \n",i,(long)getpid(),(long)getppid(),(long)childone,(long)childtwo);
			//once the parent prints it sleeps again so that the children have time to print 
			//this prevents the prompt from displaying inbetween the prints
			sleep(i);
			}
		}
		if (childone==-1 || childtwo==-1){//error check for the children
				perror("\n The fork failed \n");
				exit(1);
		}				
	       if(wait(NULL)>0)break;//waits until the child process is done before exiting 
	}
	exit(0);
}  
